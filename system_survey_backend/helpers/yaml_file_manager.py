import yaml



def write_yaml(path, content):

    #---CONTENT FORMAT---#
    # content = {
    #     'item' : {
    #         'attr': '<value>'
    #     }
    # }

    with open(path, 'w') as file:
        yaml.dump(content, file, default_flow_style=None)


def read_yaml(path):

    with open(path, 'r') as file:
        file_content = yaml.safe_load(file)

    return file_content

