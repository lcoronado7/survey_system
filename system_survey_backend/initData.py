import os
import random


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'survey_system.settings')
import django
django.setup()

from survey_system_api.repositories.surveys import SurveyRepository
from survey_system_api.repositories.answers import AnswerRepository
from survey_system_api.repositories.questions import QuestionRepository



survey = SurveyRepository()

questions =[
    {
        "question_text":"Correo electronico",
        "alias_answer":"email",
        "type_answer":"email",
        "values": None
    },
    {
        "question_text":"Seleccione su rango de edad",
        "alias_answer":"age_range",
        "type_answer":"option",
        "values": ['18-25','26-33','34-40','41+']
    },
    {
        "question_text":"Seleccione su genero",
        "alias_answer":"gender",
        "type_answer":"option",
        "values": ['M','F']
    },
    {
        "question_text":"Red Social Favorita",
        "alias_answer":"favorite_social_network",
        "type_answer":"option",
        "values": ['Facebook','Whatsapp','Twitter','Instagram','TikTok']
    },
    {
        "question_text":"Tiempo Promedio de Uso de Facebook",
        "alias_answer":"average_usability_facebook",
        "type_answer":"option",
        "values": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
    },
    {
        "question_text":"Tiempo Promedio de Uso de WhatsApp",
        "alias_answer":"average_usability_whatsapp",
        "type_answer":"option",
        "values": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
    },
    {
        "question_text":"Tiempo Promedio de Uso de Twitter",
        "alias_answer":"average_usability_twitter",
        "type_answer":"option",
        "values": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
    },
    {
        "question_text":"Tiempo Promedio de Uso de Instagram",
        "alias_answer":"average_usability_instagram",
        "type_answer":"option",
        "values": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
    },
    {
        "question_text":"Tiempo Promedio de Uso de TikTok",
        "alias_answer":"average_usability_tiktok",
        "type_answer":"option",
        "values": [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
    }
    
]
survey.create_survey_and_questions("SOCIAL NETWORK SURVEY", "DESCRIBE SOCIAL NETWORK USABILITY",questions)