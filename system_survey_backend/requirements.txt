django==3.2.4
djangorestframework==3.12.4
django-seed==0.3.1
django-cors-headers==3.10.0
gunicorn==20.1.0
pyyaml==6.0