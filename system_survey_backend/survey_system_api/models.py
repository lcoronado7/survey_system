from django.db import models
from django.utils import timezone
import json

class Survey(models.Model):
    
    title = models.CharField(max_length=255)
    desc = models.TextField(null=True,default=None)
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        
        return self.title


class Question(models.Model):
    
    TEXT = 'TEXT'
    OPTION = 'OPTION'
    NUMBER = 'NUMBER'
    EMAIL = 'EMAIL'

    TYPE_OPTIONS = (
        (TEXT,"text"),
        (OPTION,"option"),
        (NUMBER,"number"),
        (EMAIL,"email")
    )

    survey = models.ForeignKey(Survey, on_delete=models.CASCADE)
    question_text = models.TextField()
    alias_answer = models.CharField(max_length=255,default=None)
    type_answer = models.CharField(max_length=255,
        choices = TYPE_OPTIONS,
        default = TEXT)

    values = models.TextField(null=True,default=None)


    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now_add=True)

    @property
    def values_array(self):
        values_convert = None
        if(self.values):
            values_convert = self.values.replace("'",'"')
            values_convert = json.loads(values_convert)
        return values_convert


    def __str__(self):
        
        return self.question_text

class Participant(models.Model):
    
    identification_field = models.EmailField(max_length = 254,unique=True)

    def __str__(self):
        
        return self.identification_field

        
class Answer(models.Model):

    answers_code = models.CharField(max_length = 300)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE,null=True,default=None)
    answer_content = models.TextField()
    created_at  = models.DateTimeField(auto_now_add=True)
    updated_at  = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['question_id', 'answers_code'], name='participant_survey_replies')
        ]
    
    def __str__(self):
        
        return self.answer_content