from survey_system_api.models import Answer  
from survey_system_api.repositories.questions import QuestionRepository
from survey_system_api.repositories.participants import ParticipantRepository

from django.db.models import Sum, Count,Avg
from django.db.models import Q
import uuid

class AnswerRepository:

    def __init__(self):
        self.participantRepository = ParticipantRepository() 
        self.questionRepository = QuestionRepository() 

    def create_answer(self,question_id,identification_field, answerd_text):

        answer = None

        participant,created =  self.participantRepository.create_or_get_participant(identification_field)

        question = self.questionRepository.get_question_by_id(question_id)

        answers_code = uuid.uuid4()

        if(participant and question):

            answer = Answer.objects.create(answers_code=answers_code,participant=participant,question=question,answerd_text=answerd_text)
                

        return answer

    def save_answers_survey(self,identification_field,answers):

        answers_saved = False
        try:
            participant,created =  self.participantRepository.create_or_get_participant(identification_field)
            answers_code = uuid.uuid4()

            for answer in answers:
                
                question = self.questionRepository.get_question_by_id(answer['question_id'])
                
                if(question):

                    Answer.objects.create(answers_code=answers_code,participant=participant,question=question,answer_content=answer['answer_content'])
            
            return True,None

        except Exception as e:
            print(e)
            return False,e


    
    def get_answers_by_question(self, question_id):
        
        answers = None
        answers = Answer.objects.filter(question_id=question_id)

        return answers

    def get_answer_average(self, question_id):

        answer_average = 0


        answer_average = Answer.objects.filter(question_id=question_id,question__type_answer="number").values('answer_content').annotate(average=Avg('answer_content'))

        return answer_average
    
    
    def get_answers_counter(self, question_id):

        question = self.questionRepository.get_question_by_id(question_id)

        answers_counters = Answer.objects.filter(question__survey_id=question.survey_id,question_id=question_id,question__type_answer="option").values('answer_content').annotate(amount=Count('answer_content'))
        
        options = question.values_array

            
        if(options):
            
            return answers_counters,options
        
        
        return answers_counters, False

    def count_total_surveys_replied(self,survey_id):
        
        total_replies = 0

        question_replied = Answer.objects.filter(question__survey_id=survey_id).values('question_id').annotate(total_replies=Count('question_id'))
        
        if(question_replied):
            total_replies = question_replied[0]['total_replies']

        return total_replies

    def count_participant_answers(self,survey_id):

        question_replied = Answer.objects.filter(question__survey_id=survey_id).values('participant_id').annotate(amount=Count('participant_id'))
        
        return question_replied
    
    def get_survey_participants(self,survey_id):

        amount_participant= 0
        participants_survey = Answer.objects.filter(question__survey_id=survey_id).values('participant_id').annotate(amount=Count('participant_id'))
        
        if(participants_survey):
            
            amount_participant = len(participants_survey)

        return amount_participant 

    def groped_and_count_questions(self,survey_id,base_question_id,to_count_question_id):

        counter_options = {}
        try:
            name_map = {'question1': 'question1', 'question2': 'question2', 'count_question': 'count_question','id':'id'}
            query = ('''select id,question1,question2, count(question2) as count_question FROM ( select 
            max(case when t1.question_id ='''+str(base_question_id)+''' then t1.answer_content end) question1,
            max(case when t1.question_id ='''+str(to_count_question_id)+''' then t1.answer_content end) question2,
            t1.id as id
            from survey_system_api_answer as t1 INNER JOIN survey_system_api_question as qtable 
            ON t1.question_id=qtable.id  WHERE qtable.survey_id='''+str(survey_id)+'''
            group by t1.answers_code)  GROUP BY question1,question2''')
            
            # print(query)

            answers = Answer.objects.raw(query,name_map)
            
            # columns = answers.columns
            
            if(answers):
                for row in answers:
                    # print(tuple(getattr(row, col) for col in columns))
                    

                    if(row.question1 in counter_options.keys()):
                        currentTotal = counter_options[row.question1]['total'] + row.count_question
                        if(row.count_question>counter_options[row.question1]['counter']):
                            
                            counter_options[row.question1]={'option':row.question2,'counter':row.count_question,'total':currentTotal}
                        else:
                            counter_options[row.question1]['total'] = currentTotal
                    else:
                        counter_options[row.question1]={'option':row.question2,'counter':row.count_question, 'total':row.count_question}

        except Exception as e:
            print(e)
            return counter_options,e

        return counter_options, False



    def groped_and_average_question(self,survey_id,base_question_id,to_average_question_id,base_option):

        try:
            name_map = {'question1': 'question1', 'time_avg': 'time_avg','id':'id'}
            query = ('''select id,question1,avg(question2) as time_avg FROM ( select 
            max(case when t1.question_id ='''+str(base_question_id)+''' then t1.answer_content end) question1,
            max(case when t1.question_id ='''+str(to_average_question_id)+''' then t1.answer_content end) question2,
            t1.id as id
            from survey_system_api_answer as t1 INNER JOIN survey_system_api_question as qtable 
            ON t1.question_id=qtable.id  WHERE qtable.survey_id='''+str(survey_id)+'''
            group by t1.answers_code) WHERE question1="'''+str(base_option)+'''" GROUP BY question1''')

            # print(query)

            answers = Answer.objects.raw(query,name_map)
            # columns = answers.columns
            average_options = {}
            if(answers):
                for row in answers:
                    average_options[row.question1]=row.time_avg
                
        except Exception as e:
            print(e)
            return average_options,e

        return average_options, False

    def groped_and_average_questions(self,survey_id,base_question_id,to_average_questions_id_list):

        average_options = None

        try:

            base_question = self.questionRepository.get_question_by_id(base_question_id) 

            if(base_question and base_question.type_answer=="option"):
                bases_options = base_question.values_array

                if(bases_options and to_average_questions_id_list and len(bases_options)==len(to_average_questions_id_list)):
                    # for to_average_question_id in to_average_questions_id_list:
                    counter = 0
                    average_options = {}
                    name_map = {'question1': 'question1', 'time_avg': 'time_avg','id':'id'}

                    for base_option in bases_options:
                        # print(base_option,to_average_questions_id_list[counter])
                        

                        query = ('''select id,question1,avg(question2) as time_avg FROM ( select 
                        max(case when t1.question_id ='''+str(base_question_id)+''' then t1.answer_content end) question1,
                        max(case when t1.question_id ='''+str(to_average_questions_id_list[counter])+''' then t1.answer_content end) question2,
                        t1.id as id
                        from survey_system_api_answer as t1 INNER JOIN survey_system_api_question as qtable 
                        ON t1.question_id=qtable.id  WHERE qtable.survey_id='''+str(survey_id)+'''
                        group by t1.answers_code) WHERE question1="'''+str(base_option)+'''" GROUP BY question1''')


                        # print(query)

                        answers = Answer.objects.raw(query,name_map)
                        # columns = answers.columns
                        for row in answers:
                            average_options[row.question1]=round(row.time_avg,2)
                            # print(row.question1,row.time_avg)
                            # print(tuple(getattr(row, col) for col in columns))

                        counter = counter + 1

        except Exception as e:
            print(e)
            return average_options,e

        
        return average_options, False





    



  


        
        
