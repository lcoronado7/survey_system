from survey_system_api.models import Participant  

class ParticipantRepository:


    def create_or_get_participant(self,identification_field):
        
        participant,created = Participant.objects.get_or_create(identification_field=identification_field)

        return participant,created