from survey_system_api.models import Question  
import json

class QuestionRepository:


    def create_question(self,survey,question_text, alias_answer,type_answer, values=None):

        if(values):
            values=json.dumps(values)

        question_object = Question.objects.create(survey=survey,question_text=question_text,alias_answer=alias_answer,type_answer=type_answer,values=values)
      
        return question_object

    def create_questions(self,survey,questions):
        created_questions = []

        for question in questions:

            created_question = Question.objects.create(survey=survey,question_text=question['question_text'],alias_answer=question['alias_answer'],type_answer=question['type_answer'], values=question['values'])
           
            created_questions.append(created_question)


    def get_question_by_id(self,question_id):

        question = Question.objects.get(pk=question_id)

        return question

    def get_questions_by_survey(self,survey_id):

        questions = Question.objects.filter(survey_id=survey_id)

        

        return questions

    
            