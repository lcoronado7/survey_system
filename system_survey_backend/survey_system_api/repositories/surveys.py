from survey_system_api.models import Survey
from survey_system_api.repositories.questions import QuestionRepository


class SurveyRepository:

    def __init__(self):
        self.question_repository = QuestionRepository()

    def create_survey(self,title,desc):
        survey_object = Survey.objects.create(title=title,desc=desc)

        return survey_object

    def create_survey_and_questions(self,title,desc,questions):

       survey_object = Survey.objects.create(title=title,desc=desc)
       survey_questions = self.question_repository.create_questions(survey_object,questions)

       return survey_questions

    def get_survey_by_id(self,survey_id):
        survey = Survey.objects.get(pk=survey_id)


        return survey

    
    def get_survey_questions(self,survey_id):
        
        questions_survey = []
        
        survey_object = Survey.objects.get(pk=survey_id)

        if(survey_object):

            questions_survey = self.question_repository.get_questions_by_survey(survey_object)

        return questions_survey


