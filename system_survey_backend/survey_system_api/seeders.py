from django_seed import Seed
from models import Question, Survey


def surveySeeders():
    seeder = Seed.seeder()
    seeder.add_entity(Survey, 5)
    seeder.execute()


def questionSeeders():
    seeder = Seed.seeder()

    questionArray = [{

    }]
    seeder.add_entity(Question, 5)
    seeder.execute()