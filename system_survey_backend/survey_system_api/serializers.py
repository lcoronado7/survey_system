from rest_framework import serializers
from survey_system_api.models import Question, Survey, Answer, Participant

class SurveySerializer(serializers.ModelSerializer):

    class Meta:
        model = Survey
        fields="__all__"


class QuestionSerializer(serializers.ModelSerializer):
    values_array = serializers.ReadOnlyField()
    class Meta:
        model = Question
        fields="__all__"


class AnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Answer
        fields="__all__"



class ParticipantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Participant
        fields="__all__"
