from django.urls import path,include
from survey_system_api import views


urlpatterns = [
    path('save-answers',views.save_answers,name="save-answers"),
    path('get-survey/<str:pk>',views.get_survey,name="get-survey"),
    path('survey-questions/<str:survey_id>',views.get_survey_questions,name="get-survey-questions"),
    path('get-count-answer-group/<str:question_id>',views.count_group_question_answers,name="get-count-answer-group"),
    path('get-total-participants-survey/<str:survey_id>',views.get_total_participants_survey,name="get-total-participants-survey"),
    path('get-total-survey-replies/<str:survey_id>',views.get_total_survey_replies,name="get-total-survey-replies"),

    path('count-answers-by-options/<str:survey_id>/<str:base_question_id>/<str:to_count_question_id>',views.count_answers_groped_by_options,name="count-answers-by-options"),
    path('average-answer-by-option/<str:survey_id>/<str:base_question_id>/<str:to_average_question_id>/<str:base_option>',views.average_answer_grouped_by_option,name="average-answer-by-option"),
    path('averages-answers-by-options',views.averages_answers_grouped_by_options,name="averages-answers-by-options")
]