from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from survey_system_api.repositories.surveys import SurveyRepository
from survey_system_api.repositories.questions import QuestionRepository
from survey_system_api.repositories.answers import AnswerRepository
from survey_system_api.serializers import *
from django.core import serializers




@api_view(['GET'])
def get_survey(request,pk):
    
    surveyRepository = SurveyRepository()
    survey_object=surveyRepository.get_survey_by_id(pk)

    if(survey_object):

        survey_object= SurveySerializer(survey_object)

        return Response(survey_object.data,status=200)

    return Response(status=400)


@api_view(['GET'])
def get_survey_questions(request,survey_id):
    print(survey_id)

    questionRepository = QuestionRepository()
    question_object=questionRepository.get_questions_by_survey(survey_id)

    if(question_object):

        question_object= QuestionSerializer(question_object, many=True)
        return Response(question_object.data,status=200)
        
    return Response(status=400)



@api_view(['GET'])
def get_question_answers_average(request,question_id):
    print(question_id)

    answerRepository = AnswerRepository()
    answer_object=answerRepository.get_answer_average(question_id)

    if(answer_object):

        return Response(answer_object,status=200)
        
    return Response(status=400)


@api_view(['GET'])
def count_group_question_answers(request,question_id):

    objectResponse = {}
    answerRepository = AnswerRepository()
    answers_objects, options =answerRepository.get_answers_counter(question_id)

    if(answers_objects and options):
        objectResponse = {}
        for option in options:
            result_option = list(filter(lambda counter: counter['answer_content']==option, answers_objects)) 
            amount = result_option[0]['amount'] if result_option else 0

            objectResponse[option]= amount
    elif(options):
        objectResponse = {}
        for option in options:
            objectResponse[option]= 0

    if(objectResponse):
        return Response(objectResponse,status=200)
        
    return Response(status=400)


@api_view(['GET'])
def get_total_participants_survey(request,survey_id):
    print(survey_id)

    answerRepository = AnswerRepository()
    total_participant=answerRepository.get_survey_participants(survey_id)

    if(total_participant!=None):

        return Response({"survey_id":survey_id ,"total":total_participant},status=200)
        
    return Response(status=400)

@api_view(['GET'])
def get_total_survey_replies(request,survey_id):
    print(survey_id)

    answerRepository = AnswerRepository()
    total_replies=answerRepository.count_total_surveys_replied(survey_id)

    if(total_replies!=None):

        return Response({"survey_id":survey_id ,"total_replies":total_replies},status=200)
        
    return Response(status=400)


@api_view(['POST'])
def save_answers(request):

    identification_field = request.data['identification_field']
    answers = request.data['answers']
    answerRepository = AnswerRepository()
    result, errors=answerRepository.save_answers_survey(identification_field,answers)

    if(result):
        responseObject ={
            "message":"Answers Created", 
        }
    else:
        responseObject ={
            "message":"Answers no Created", 
            "erros":errors
        }

    if(result):

        return Response(responseObject,status=200)
        
    return Response(status=400)



@api_view(['GET'])
def count_answers_groped_by_options(request,survey_id,base_question_id,to_count_question_id):
    
    answerRepository = AnswerRepository()
    result, errors=answerRepository.groped_and_count_questions(survey_id,base_question_id,to_count_question_id)

    if(result):
        responseObject = result
    else:
        responseObject ={
            "message":'Error to get counters', 
            "erros":errors
        }

    if(result):

        return Response(responseObject,status=200)
        
    return Response(status=400)

@api_view(['GET'])
def average_answer_grouped_by_option(request,survey_id,base_question_id,to_average_question_id,base_option):

    answerRepository = AnswerRepository()
    result, errors=answerRepository.groped_and_average_question(survey_id,base_question_id,to_average_question_id,base_option)

    if(result):
        responseObject = result
    else:
        responseObject ={
            "message":'Error to get counters', 
            "erros":errors
        }

    if(result):

        return Response(responseObject,status=200)
        
    return Response(status=400)


@api_view(['POST'])
def averages_answers_grouped_by_options(request):
    
    survey_id = request.data['survey_id']
    base_question_id = request.data['base_question_id']
    to_average_questions_id_list = request.data['to_average_questions_id_list']
    answerRepository = AnswerRepository()
    result, errors=answerRepository.groped_and_average_questions(survey_id,base_question_id,to_average_questions_id_list)

    if(result):
        responseObject = result
    else:
        responseObject ={
            "message":'Error to get averages', 
            "erros":errors
        }

    if(result):

        return Response(responseObject,status=200)
        
    return Response(status=400)
