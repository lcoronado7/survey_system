import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);


const router = new VueRouter({
    mode: 'history',
    routes:[
      {
      path:"/",
      name:'home',
      component:require('../views/home.vue').default
      },
      {
          path: '/survey/create',
          name: 'createSurvey',
          component: require('../views/surveys.vue').default
      },
      {
          path: '/survey/statistics',
          name: 'statisticsSurvey',
          component: require('../views/stadistics.vue').default
      }
    ]
})

export default router
