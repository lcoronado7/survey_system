import Request from '../../helpers/Request.js';
import BaseUrl from '../../helpers/BaseUrl.js';


export async function saveAnswers(context,data){
  const url = BaseUrl.getUrl('save-answers');
  const request = await Request.make('post', url,data);
  return request;

}

export async function getQuestions(context,survey_id) {
  const url = BaseUrl.getUrl('survey-questions/'+survey_id);
  const request = await Request.make('get', url);

  if(request.status==200){context.commit('setProperty', {key: 'questions', data: request.data});}
  return request;
}

export async function getSurvey(context,survey_id) {
  const url = BaseUrl.getUrl('get-survey/'+survey_id);
  const request = await Request.make('get', url);

  if(request.status==200){context.commit('setProperty', {key: 'survey', data: request.data});}
  return request;
}

export async function getTotalParcipantsSurvey(context,survey_id) {
  const url = BaseUrl.getUrl('get-total-participants-survey/'+survey_id);
  const request = await Request.make('get', url);

  if(request.status==200){context.commit('setProperty', {key: 'survey', data: request.data});}
  return request;
}

export async function getSurveyTotalReplies(context,survey_id) {
  const url = BaseUrl.getUrl('get-total-survey-replies/'+survey_id);
  const request = await Request.make('get', url);

  return request;
}

export async function getAnswerCounterGroup(context,question_id) {
  const url = BaseUrl.getUrl('get-count-answer-group/'+question_id);
  const request = await Request.make('get', url);

  if(request.status==200){context.commit('setProperty', {key: 'counters', data: request.data});}
  return request;
}


export async function countAnswersGroupedByOptions(context,data) {
  const url = BaseUrl.getUrl('count-answers-by-options/'+data.survey_id+"/"+data.base_question_id+"/"+data.to_count_question_id);
  const request = await Request.make('get', url);

  
  return request;
}

export async function averageAnswerGropedByOption(context,data) {
  const url = BaseUrl.getUrl('average-answer-by-option/'+data.survey_id+"/"+data.base_question_id+"/"+data.to_average_question_id+"/"+data.base_option);
  const request = await Request.make('get', url);

  
  return request;
}

export async function averagesAnswersGropedByOptions(context,data) {
  const url = BaseUrl.getUrl('averages-answers-by-options');
  const request = await Request.make('post', url,data);


  return request;
}
